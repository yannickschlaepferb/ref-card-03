# App Ref. Card 03
Standalone Spring Boot Application

---

### Prerequisites

* You have installed java 11
* You have installed the latest version of Maven
* You have a MariaDB Database running

### Installation
Fork Ref Card03 and clone it on your computer

### Steps before automation

1. Create a Repository
2. Create an image with the tag latest and push it into the Repository
3. Create an RDS Database(make sure to save the password if you generated one)
4. Create a Cluster
5. Create a Task Definition with these environment variables
* DB_URL 
* DB_USERNAME 
* DB_PASSWORD
6. Create a Service with a loadbalancer
7. Make sure to add all of these variables to the gitlab CI/CD
* AWS_ACCESS_KEY_ID (found in AWS Details after clicking "show")
* AWS_SECRET_ACCESS_KEY  (found in AWS Details after clicking "show")
* AWS_SESSION_TOKEN  (found in AWS Details after clicking "show")
* AWS_CLUSTER (Name of your Cluster)
* AWS_DEFAULT_REGION (Default region of your AWS)
* AWS_SERVICE (Name of your Service)
* CI_AWS_ECR_REGISTRY (URL of your Registry. you get it by clicking on the copy Icon under URL)
* CI_AWS_ECR_REPOSITORY_NAME (name of your repository)

### If something is unclear
Check out my documentation in this repository, it will explain most of the mentioned things in AWS.

